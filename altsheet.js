import { registerHandlebarsHelpers, registerWithAIP } from "./module/helpers.js";

Hooks.once("init", () => {
    const templates = [
        "modules/pf1-alt-sheet/templates/altsheet.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-details.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-attributes.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-attacks.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-buffs.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-features.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-inventory.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-skills-front.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-skills.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-spellbook-front.hbs",
        "modules/pf1-alt-sheet/templates/parts/actor-spellbook.hbs"
    ];
    loadTemplates(templates);

    registerHandlebarsHelpers();

    registerWithAIP();

    console.log("pf1-alt-sheet | loaded");
});

Hooks.on("ready", async() => {
    const AltSheetMixin = (await import("./module/sheets.js")).AltSheetMixin;

    const AltActorSheetPFCharacter = (await import("./module/sheets.js")).AltActorSheetPFCharacter;
    Object.assign(AltActorSheetPFCharacter.prototype, AltSheetMixin);
    Actors.registerSheet("pf1alt", AltActorSheetPFCharacter, {
        label: "PF1AS.CharacterSheetLabel",
        types: ["character"],
        makeDefault: false
    });

    const AltActorSheetPFNPC = (await import("./module/sheets.js")).AltActorSheetPFNPC;
    Object.assign(AltActorSheetPFNPC.prototype, AltSheetMixin);
    Actors.registerSheet("pf1alt", AltActorSheetPFNPC, {
        label: "PF1AS.NPCSheetLabel",
        types: ["npc"],
        makeDefault: false
    });

    console.log("pf1-alt-sheet | sheets registered");
});
